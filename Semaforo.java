package compito;

public class Semaforo {

	int timer = 50, timerAttuale = 0;
	String colore = "rosso";
	
	public Semaforo(int ritardo) {
		this.timer = ritardo;
	}
	
	public void start() {
		while (true) {
		    if (timerAttuale < 0) {
		    	cambiaColore();
		    	System.out.println(this);
		        timerAttuale = timer;
		    }
		    timerAttuale--;
		}
	}
	
	private void cambiaColore() {
		switch(colore) {
		case "rosso":
		default:
			colore = "verde";
			break;
		case "giallo":
			colore = "rosso";
			break;
		case "verde":
			colore = "giallo";
			break;
		}
	}
	
	public String toString() {
		return "Il semaforo � " + colore;
	}
	
}
